

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'binding.poc.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'binding.poc.UserRole'
grails.plugin.springsecurity.authority.className = 'binding.poc.Role'
grails.plugin.springsecurity.requestMap.className = 'binding.poc.Requestmap'
grails.plugin.springsecurity.securityConfigType = 'Requestmap'
grails.plugin.springsecurity.roleHierarchyEntryClassName = 'binding.poc.RoleHierarchyEntry'
grails.plugin.springsecurity.password.algorithm='SHA-512'
grails.plugin.springsecurity.password.hash.iterations=1
grails.plugin.springsecurity.error.login.fail="Check"
grails.plugin.springsecurity.errors.login.passwordExpired = "Your password has expired. Please reset your password."
grails.plugin.springsecurity.successHandler.alwaysUseDefault = true
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/user/index'
grails.plugin.springsecurity.rejectIfNoRule = false
grails.plugin.springsecurity.fii.rejectPublicInvocations = false
grails.plugin.springsecurity.auth.loginFormUrl = '/admin/login'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.adh.errorPage = '/error/index'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]

