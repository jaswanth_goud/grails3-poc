package binding.poc

class BootStrap {

    def init = { servletContext ->
        def authorities = ['ROLE_CLIENT', 'ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_MODERATOR', 'ROLE_MANAGER']
        authorities.each {
            if ( !Role.findByAuthority(it) ) {
                new Role(authority: it).save()
            }
        }
        if ( !User.findByUsername('admin') ) {
            def u = new User(username: 'admin', password: 'P@ssword01', firstName: 'Admin', lastName: 'Admin', gender: 'M', dob: '22/08/1957', address: 'His Address', phoneNumber: '1234567890')
            u.save()
            def ur = new UserRole(user: u, role:  Role.findByAuthority('ROLE_ADMIN'))
            ur.save()
            def ur1 = new UserRole(user: u, role:  Role.findByAuthority('ROLE_CLIENT'))
            ur1.save()
            def ur2 = new UserRole(user: u, role:  Role.findByAuthority('ROLE_STAFF'))
            ur2.save()
            def ur3 = new UserRole(user: u, role:  Role.findByAuthority('ROLE_MODERATOR'))
            ur3.save()
            def ur4 = new UserRole(user: u, role:  Role.findByAuthority('ROLE_MANAGER'))
            ur4.save()
        }

        if ( !User.findByUsername('client') ) {
            def u = new User(username: 'client', password: 'P@ssword01', firstName: 'Client', lastName: 'Client', gender: 'M', dob: '12/02/1927', address: 'His Address', phoneNumber: '1234567890')
            u.save()
            def ur = new UserRole(user: u, role:  Role.findByAuthority('ROLE_CLIENT'))
            ur.save()
        }
        if ( !User.findByUsername('staff') ) {
            def u = new User(username: 'staff', password: 'P@ssword01', firstName: 'Staff', lastName: 'Staff', gender: 'M', dob: '22/08/1957', address: 'His Address', phoneNumber: '1234567890')
            u.save()
            def ur = new UserRole(user: u, role:  Role.findByAuthority('ROLE_STAFF'))
            ur.save()
        }
        if ( !User.findByUsername('moderator') ) {
            def u = new User(username: 'moderator', password: 'P@ssword01', firstName: 'Moderator', lastName: 'Moderator', gender: 'F', dob: '02/01/1987', address: 'Her Address', phoneNumber: '1234567890')
            u.save()
            def ur = new UserRole(user: u, role:  Role.findByAuthority('ROLE_MODERATOR'))
            ur.save()
        }
        if ( !User.findByUsername('manager') ) {
            def u = new User(username: 'manager', password: 'P@ssword01', firstName: 'Manager', lastName: 'Manager', gender: 'M', dob: '05/11/1999', address: 'His Address', phoneNumber: '1234567890')
            u.save()
            def ur = new UserRole(user: u, role:  Role.findByAuthority('ROLE_MANAGER'))
            ur.save()
        }
        if ( !User.findByUsername('admin') ) {
            def u = new User(username: 'admin', password: 'P@ssword01', firstName: 'Admin', lastName: 'Admin', gender: 'M', dob: '22/08/1957', address: 'His Address', phoneNumber: '1234567890')
            u.save()
            def ur = new UserRole(user: u, role:  Role.findByAuthority('ROLE_ADMIN'))
            ur.save()
        }
    }
    def destroy = {
    }
}
