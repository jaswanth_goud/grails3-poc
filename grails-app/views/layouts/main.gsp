<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:javascript src="application.js"/>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="style4.css"/>
    <g:layoutHead/>
</head>
<body>

    %{--<div class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/#">
                    <i class="fa grails-icon">
                        <asset:image src="grails-cupsonly-logo-white.svg"/>
                    </i> Grails
                </a>
            </div>
            <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">
                <ul class="nav navbar-nav navbar-right">
                    <g:pageProperty name="page.nav" />
                </ul>
            </div>
        </div>

    </div>--}%

    <!-- Sidebar Holder -->
<div class="wrapper1">

    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <div>
                <h3>Bootstrap Sidebar</h3>
            </div>
            <div>
                <strong>BS</strong>
                <a href="#" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-align-left"></i>
                </a>
            </div>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <a href="${request.contextPath + '/user/index'}">
                    <i class="glyphicon glyphicon-home"></i>
                    Users
                </a>
                %{--<ul class="collapse list-unstyled" id="homeSubmenu">
                    <li><a href="#">Home 1</a></li>
                    <li><a href="#">Home 2</a></li>
                    <li><a href="#">Home 3</a></li>
                </ul>--}%
            </li>
            <li  class="">
                <a href="${request.contextPath + '/roles/index'}">
                    <i class="glyphicon glyphicon-briefcase"></i>
                    Roles
                </a>
                %{--<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="glyphicon glyphicon-duplicate"></i>
                    Pages
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><a href="#">Page 1</a></li>
                    <li><a href="#">Page 2</a></li>
                    <li><a href="#">Page 3</a></li>
                </ul>--}%
            </li>
            %{--<li>
                <a href="#">
                    <i class="glyphicon glyphicon-link"></i>
                    Portfolio
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="glyphicon glyphicon-paperclip"></i>
                    FAQ
                </a>
            </li>--}%
            <li class="">
                <a href="#">
                    <i class="glyphicon glyphicon-send"></i>
                    Contact
                </a>
            </li>
        </ul>


    </nav>
    <div id="header">
        <!-- TOP NAVIGATION MENU -->
        <div class="navbar navbar-inverse navbar-fixed-top1">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a href="#" class="pull-right dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-align-justify"></i>
                    </a>
                   %{-- <button type="button" class="btn btn-navbar pull-right dropdown-toggle" data-toggle="dropdown">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>--}%
                    <a class="brand" href="#">${session['page']}</a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#"><i class="icon-user"></i> Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="${request.contextPath + '/logout/index'}"><i class="icon-off"></i> Log Out</a></li>
                    </ul>

                </div>
            </div>
        </div>
        <g:layoutBody/>
    </div>

</div>



    %{--<div class="footer" role="contentinfo"></div>--}%

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <asset:javascript src="application.js"/>

</body>
</html>
