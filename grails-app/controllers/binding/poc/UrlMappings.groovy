package binding.poc

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/admin/login"{
            controller = "login"
            action = "auth"
            userType = "admin"
        }

        "/"{
            controller = "login"
            action = "auth"
            userType = "admin"
        }
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
