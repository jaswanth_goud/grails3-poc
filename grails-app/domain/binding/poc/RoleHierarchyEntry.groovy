package binding.poc

import java.io.Serializable;

class RoleHierarchyEntry implements Serializable {

	private static final long serialVersionUID = 1

	String entry

	RoleHierarchyEntry(String entry) {
		this()
		this.entry = entry
	}

	@Override
	int hashCode() {
		entry?.hashCode() ?: 0
	}

	@Override
	boolean equals(other) {
		is(other) || (other instanceof RoleHierarchyEntry && other.entry == entry)
	}

	@Override
	String toString() {
		entry
	}

	static constraints = {
		entry blank: false, unique: true
	}

	static mapping = {
		cache true
	}
}
