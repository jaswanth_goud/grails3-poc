package binding.poc

class RolesController {

    def index() {
        session['page'] = 'Roles'
        [rolesLst: Role.findAll()]
    }
}
