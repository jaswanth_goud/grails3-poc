package binding.poc

class UserRoleController {

    def index() {
        session['page'] = 'UserRoles'
        def userIns = User.findByUsername(params.username)
        def userRolesList = UserRole.findAllByUser(userIns)
        [userRolesList: userRolesList]
    }
}
