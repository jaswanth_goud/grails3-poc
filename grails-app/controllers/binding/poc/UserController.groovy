package binding.poc

class UserController {

    def index() {
        session['page'] = 'Users'
        def userLst = User.findAll()
        [usersList: userLst]
    }
}
