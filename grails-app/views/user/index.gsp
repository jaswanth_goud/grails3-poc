<%--
  Created by IntelliJ IDEA.
  User: Jaswanth
  Date: 9/4/2017
  Time: 3:19 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Users list</title>
    <meta name="layout" content="${gspLayout ?: 'main'}"/>
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'styles.css')}" type="text/css">--}%
    <!-- Our Custom CSS -->
    <asset:stylesheet src="style4.css"/>
    <style type="text/css" media="screen">
        .panel {
            box-shadow: 0px 0px 0px rgba(0,0,0,0.05);
        }
    </style>

</head>

<body>

<!-- /.row -->
<div class="wrapper">

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                User Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Date of Birth</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Errors</th>

                        </tr>
                        </thead>

                        <tbody id="usr_tbl">
                        <g:each in="${usersList}" var="user">
                            <tr >
                                <td ><a href="#" onclick='document.location = "<g:createLink controller='userRole' action='index' params="${[username: user.username]}"/>" '><u> ${user.username}</u></a></td>
                                <td>${user.firstName}</td>
                                <td>${user.lastName}</td>
                                <td>${user.dob}</td>
                                <td>${user.gender}</td>
                                <td>${user.address}</td>
                                <td>${user.phoneNumber}</td>
                                <td><a href="#">Error</a></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="modal fade customModal createBoardModal" id="createBoard" tabindex="-1" role="dialog" aria-labelledby="createBoard_FormLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                Header
            </div>
            <div class="modal-body p-a-0">
                <!-- <create-careplace></create-careplace> -->
                <div class="createCplace_Div p-x-15 p-y-10">
                    <div class="alert alert-danger alert-dismissable">This is error message</div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#usr_tbl tr td a").on('click', function(){
           $(".modal").modal('show')
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });

        function getUserRoles(user){
            alert('role:'+user.id)
        }
    });
    function getUserRoles(username){
        alert('role:'+username)
    }

</script>
</body>
</html>